const KenoEngine = require('keno-core');
const _          = require('lodash');

const printSummary = (numbers, drawnNumbers) => {
  let correctNumbers = _.intersection(numbers, drawnNumbers);
  console.log(
`
Drawn numbers: ${drawnNumbers}
Your numbers: ${numbers},
Correct numbers: ${correctNumbers}`
  );
};

let numbers = _.chain(process.argv[2].split(','))
  .uniq()
  .map((value) => parseInt(value))
  .sortBy()
  .value();

printSummary(numbers, KenoEngine.getNumbers());



